
import generated.*;
import validatorFiles.Validator;

import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import org.jdom2.*;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import static java.lang.String.valueOf;


public class Main {
    final static String FILENAME=System.getProperty("user.dir")
            + File.separator + "device.xml";

    public static void main(String[] args) {

        new Validator().checkSchema();

        DeviceType deviceType = new DeviceType();
        deviceType.setTypeName("Main");
        deviceType.setCheckPeripheral(false);
        deviceType.setPowerUsage(230);
        deviceType.setCoolerAvailability(true);
        deviceType.setPorts(Ports.DVI);

        Device device1 = new Device();
        //device1.setDeviceName(DeviceName.GRAPHICS_CARD);
        //device1.setOrigin(Origin.KOREA);
        device1.setPrice(450);
        device1.setDeviceType(deviceType);
        device1.setCritical(true);


        try {
            JAXBContext jaxbContext =
                    JAXBContext.newInstance(Device.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(device1, new File(FILENAME));

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}
