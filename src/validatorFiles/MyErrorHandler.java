package validatorFiles;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;


import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;


public class MyErrorHandler extends DefaultHandler {
    private Logger logger = Logger.getLogger("validatorFiles");

    public MyErrorHandler(String log) throws IOException {
        logger.addHandler(new FileHandler());
    }

    public void warning(SAXParseException e) {
        logger.warning(getLineAddress(e) + " - " + e.getMessage());
    }

    public void error(SAXParseException e) {
        logger.info(getLineAddress(e) + " - " + e.getMessage());
    }

    public void fatalError(SAXParseException e) {
        logger.severe(getLineAddress(e) + " - " + e.getMessage());
    }

    private String getLineAddress(SAXParseException e) {
        
        return e.getLineNumber() + " : " + e.getColumnNumber();
    }
}
